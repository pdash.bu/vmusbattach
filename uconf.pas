unit UConf;

{
 Work with configuration file.

 Файл конфигурации, при его отсутствии, создается в локальном хранилище пользователя (~/.conf/*.conf).
 Можно переместить/скопировать в глобальное хранилище (/etc/*.conf) для использования всеми пользователями.
 Локальный имеет приоритет над глобальным.
 }
{$mode objfpc}{$H+}

interface


uses
  Classes, SysUtils, UObsh;

var
  // Файл настроек
  Conf_FileName: string;

function GetConfFileName: string;
function getKvmParameters(KvmName: string): string;


implementation

function getKvmParameters(KvmName: string): string;
var
  i: integer;
  s: string;
begin
  Result := '';
  s := ReadFromFile(Conf_FileName);
  // Удалить строки с комментариями
  s := DelCommects(s);

  // поиск имени в файле конфигурации
  KvmName := '[' + KvmName + ']';

  i := pos(KvmName, s);
  // не найдено - выйти
  if i <= 0 then
    exit;

  // обрезать начало конфигурации
  s := copy(s, i + length(KvmName), maxint);
  // найти следующую секцию
  i := pos('[', s) - 1;
  if i < 0 then
    i := maxInt;

  s := copy(s, 1, i);

  // получить устройства
  {  While pos(LineEnding, s) <> 0 do begin
      i := pos(LineEnding, s);
      s := copy(s, 1, i-1) + ' ' + copy(s, i+length(LineEnding), maxInt);
    end;
   }

  Result := s;
end;

procedure CreateShablon(ConfFileName: string);
var
  str: string;
begin
  begin
    str := '';
    str := str + '<hostdev mode=''subsystem'' type=''usb'' managed=''yes''>' +
      LineEnding;
    str := str + '  <source>' + LineEnding;
    str := str + '    <vendor id=''$VENDOR''/>' + LineEnding;
    str := str + '    <product id=''$PRODUCT''/>' + LineEnding;
    str := str + '    <address bus=''$BUS'' device=''$DEVICE''/>' + LineEnding;
    str := str + '  </source>' + LineEnding;
    str := str + '</hostdev>' + LineEnding;
  end;

  // создать подкаталог при отсутствии
  ForceDirectories(ExtractFilePath(ConfFileName));
  WriteToFile(GetShablonFile(ConfFileName), str);
end;

procedure CreateConfFile(ConfFileName: string);
var
  str: string;
begin
  str := '';

  str := str + '# Файл конфигурации' + LineEnding;
  str := str + '##' + LineEnding;
  str := str + '##[domain1]' + LineEnding;
  str := str + '##   vendor:product #Commect' + LineEnding;
  str := str + '##   vendor:product #Commect' + LineEnding;
  str := str + '##' + LineEnding;
  str := str + '##[domain2]' + LineEnding;
  str := str + '##   vendor:product #Commect' + LineEnding;
  str := str + '##   vendor:product #Commect' + LineEnding;
  str := str + '##' + LineEnding;
  str := str + '## vendor:product можно узнать из вывода lsusb' +
    LineEnding;
  str := str + '##' + LineEnding;
  str := str + '# Example:' + LineEnding;
  str := str + '##' + LineEnding;
  str := str + '##[VM-linux]' + LineEnding;
  str := str + '##  05e3:0608 #Genesys Logic, Inc. Hub' + LineEnding;
  str := str + '##  045e:00dd #Microsoft Corp. Comfort Curve Keyboard 2000 V1.0' +
    LineEnding;
  str := str + '##  258a:0001' + LineEnding;
  str := str + '##  03f0:002a #Hewlett-Packard LaserJet P1102' + LineEnding;


  // создать подкаталог
  ForceDirectories(ExtractFilePath(ConfFileName));
  WriteToFile(ConfFileName, str);

end;

function GetConfFileName: string;
begin
  // проверить наличие файла настроек в глобальном каталоге
  Result := GetAppConfigFile(True, True);

  // при отсутствии в глобальном каталоге проверить в локальном
  if not FileExists(Result) then
    Result := GetAppConfigFile(False, True);

  // при отсутствии везде - создать
  if not FileExists(Result) then
  begin
    // в result локальный файл. Создать его там.
    CreateConfFile(Result);
  end;

  // Создать файл-шаблон в том же каталоге при его отсутствии
  if not (FileExists(GetShablonFile(Result))) then
    CreateShablon(Result);

end;

end.
