# vmusbattach

Запуск виртуальной машины (созданной на qemu-kvm) с пробросом указанных в параметрах usb устройств.

Устройства идентифицируется парой vendor:product. Номера устройство можно узнать из вывода lsusb.
Корректно обрабатываются несколько устройств с одинаковыми параметрами vendor:product (например, джойстики HTC Vive).
Программу поместить в любой каталог из PATH, например, в /usr/bin.

Пример запуска:
vmusbattach generic 
При запуске программа укажет на необходимость прописать настройки для запуска конкретной виртуальной машины.
