unit UObsh;


{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  // файл-макет для xml-файла
  SHABLON_FILENAME = 'shablon_xml.conf';

function GetShablonFile(ConfFileName: string): string;
function DelCommects(s: string): string;

function ReadFromFile(FName: string): string;
procedure WriteToFile(fName, s: string);

implementation

function DelCommects(s: string): string;
// Удаление комментариев из текста
const
  REM = '#';
var
  l: TStrings;
  i, poss: integer;
begin
  l := TStringList.Create;
  l.Text := s;

  // обрезать строки до первого символа комментария
  for i := 0 to l.Count - 1 do
  begin
    poss := pos(REM, l[i]);
    if poss > 0 then
    begin
      l[i] := copy(l[i], 1, poss - 1);
    end;

  end;

  Result := l.Text;
  l.Free;
end;

function GetShablonFile(ConfFileName: string): string;
begin
  Result := ExtractFilePath(ConfFileName) + SHABLON_FILENAME;
end;

function ReadFromFile(FName: string): string;
var
  f: TFileStream;
begin
  f := TFileStream.Create(FName, fmopenRead);
  SetLength(Result, f.size);
  f.Read(pointer(Result)^, f.Size);
  f.Free;
end;

procedure WriteToFile(fName, s: string);
var
  f: TFileStream;
begin
  f := TFileStream.Create(FName, fmCreate);
  f.Write(pointer(s)^, length(s));
  f.Free;
end;


end.
