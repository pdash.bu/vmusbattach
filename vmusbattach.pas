program vmusbattach;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Classes,
  process,
  SysUtils,
  UConf,
  UObsh { you can add units after this };

var
  // имя виртуальной машины
  KVM_Name: string;
  // Массив USB устройств для подключения к виртуальной машине
  USB_Devices: array of string;
  // Перечень USB устройств в системе
  SYS_USB_Devices: string;

  // Дополнительное сообщение об ошибках
  ErrorString: string;

  procedure Vars_Initialization;
  // инициализация (обнуление) переменных
  begin
    KVM_Name := '';
    SetLength(USB_Devices, 0);
    ErrorString := '';
    SYS_USB_Devices := '';
  end;

  procedure ShowHelp;
  // вывести помощь по запуску программы и сообщений об ошибках
  begin
    // Вывести подробную информацию об ошибках, если она есть
    if ErrorString <> '' then
    begin
      writeln('Error:');
      writeln(ErrorString);
    end
    else
    begin
      // помощь
      writeln('Подключает указанные USB устройства к виртуальной машине.');
      writeln('Запуск: ' + ExtractFileName(ParamStr(0)) +
        ' Имя_Виртуальной_Машины');
      writeln('');
      writeln('Устройства для подкючения указываются в файле конфигурации.');
    end;
  end;

  procedure AddUsbDevice(str: string);
  // Добавить USB устройство в список для подключения
  begin
    // проверка на корректность
    // 1462:7c37 - длина = 9, пятый символ двоеточие
    if (length(str) = 9) and (str[5] = ':') then
    begin
      SetLength(USB_Devices, length(USB_Devices) + 1);
      USB_Devices[length(USB_Devices) - 1] := str;
    end
    else
    begin
      ErrorString := ErrorString + 'wrong device ' + str + #13#10;
    end;
  end;

  function ParserParamstr: boolean;
    // Проверка корректности переданных программе параметров
  var
    cnt: integer;
    l: TStrings;
    i: integer;
  begin
    Result := False;

    cnt := ParamCount;
    if cnt < 1 then
    begin
      // первый параметр - имя виртуальной машины
      exit;
    end;

    // первый параметр - имя домена kvm (виртуальной машины)
    KVM_Name := ParamStr(1);

    // Считать параметры запуска из файла конфигурации
    l := TStringList.Create;
    l.Text := getKvmParameters(KVM_Name);

    if trim(l.Text) = '' then
    begin
      ErrorString := ErrorString +
        'Настроек для виртуальной машины ' +
        KVM_Name + ' не найдено.';
      ErrorString := ErrorString +
        'Внесите данные машины в файл конфигурации.';
    end
    else
    begin
      Result := True;
    end;

    // Добавить каждое устройство в список для подкючения
    for i := 0 to l.Count - 1 do
    begin
      if l[i] <> '' then
        AddUsbDevice(trim(l[i]));
    end;

    l.Free;
  end;

  function DoProgram(ExecFileName, Params: string): string;
    // выполнение программы. Возврат результатов вывода.
  var
    i: integer;
    Proc: TProcess;
    L: TStrings;
  begin
    // преобразовать строку с пробелами в список строк, т.е. заменить пробелы на перевод строки
    Result := '';
    for i := 1 to length(Params) do
    begin
      if Params[i] <> ' ' then
        Result := Result + Params[i]
      else
        Result := Result + #13;
    end;
    Params := Result + #13;
    Result := '';

    // Запустить программу
    Proc := TProcess.Create(nil);
    Proc.Executable := ExecFileName;
    Proc.Parameters.Text := Params;
    Proc.Options := Proc.Options + [poWaitOnExit, poUsePipes];
    Proc.Execute;
    // получить вывод программы
    L := TStringList.Create;
    L.LoadFromStream(Proc.Output);
    Proc.Free;

    Result := trim(L.Text);
    L.Free;
  end;


  function Replace(s, str1, str2: string): string;
    // заменить строку str1 строкой str2
  var
    i: integer;
  begin
    // найти позцию строки
    i := pos(str1, s);
    if i > 0 then
      // вставить str2 в найденную позицию, удалив str1
    begin
      Result := copy(s, 1, i - 1) + str2 + copy(s, i + length(str1), maxInt);
    end;
  end;

  function getTempFileName(Domain: string): string;
    // сгенерировать случайное имя файла во временной папке системы
  const
    XML_EXT = '.xml';
  begin
    Randomize;
    Result := IntToStr(system.Random(maxInt));
    Result := SysUtils.GetTempDir + Domain + Result;
    Result := Result + XML_EXT;
  end;

  function CreatexmlFileName(KVM_Name, vendor, product, bus, device: string): string;
    // создать xml файл из шаблона
  const
    _VENDOR = '$VENDOR';
    _PRODUCT = '$PRODUCT';
    _BUS = '$BUS';
    _DEVICE = '$DEVICE';
  var
    s: string;
  begin
    // подставить нужные значения
    s := ReadFromFile(GetShablonFile(Conf_FileName));
    s := Replace(s, _VENDOR, vendor);
    s := Replace(s, _PRODUCT, product);
    s := Replace(s, _BUS, bus);
    s := Replace(s, _DEVICE, device);

    // получить имя файла и создать его
    Result := getTempFileName(KVM_Name);
    WriteToFile(Result, s);
  end;


  procedure getVendorProduct(str: string; var v, p: string);
  // выделить из строки значение vendor и product
  const
    PREFIX = '0x'; // начинается с этих символов
  var
    i: integer;
  begin
    // пример: "1d6b:0002". Разделяем по символу ":"
    i := pos(':', str);
    v := copy(str, 1, i - 1);
    p := copy(str, i + 1, maxInt);

    // добавить префикс
    v := PREFIX + v;
    p := PREFIX + p;
  end;

  function GetStr(str: string; num: integer): string;
    // возвращает слово из предложения (строки). Нумерация слов с 1.
  var
    i, j: integer;
  begin
    // добавляем пробел в конец, чтобы можно было найти последнее слово
    str := str + ' ';

    // пропускаем слова в начале
    for i := 1 to num - 1 do
    begin
      j := pos(' ', str);
      str := copy(str, j + 1, maxInt);
    end;

    // выделяем нужное слово
    j := pos(' ', str) - 1;
    str := copy(str, 1, j);

    // удалить все символы кроме цифр
    Result := '';
    for i := 1 to length(str) do
    begin
      if str[i] in ['0'..'9'] then
        Result := Result + str[i];
    end;
  end;

  function del0(s: string): string;
    // удалить символы "0" в начале числа
  begin
    while (length(s) > 1) and (s[1] = '0') do
      s := copy(s, 2, maxInt);

    Result := s;
  end;

  procedure getBusDevice(str: string; var b, d: string);
  begin
    // Bus 007 Device 005: ID 0bb4:030a HTC (High Tech Computer Corp.)
    // строки занимают второе и четвертое место
    b := GetStr(str, 2);
    d := GetStr(str, 4);
    // удалить нули в начале чисел.
    b := del0(b);
    d := del0(d);
  end;

  procedure AttachUSB;
  var
    i, j: integer;
    vendor, product: string;
    bus, device: string;
    xmlFileName: string;
    SYS_USB: TStrings;
    Dev: string;
    res: string;
    find: boolean;
  begin
    // получить перечень USB устройств в системе
    SYS_USB := TStringList.Create;
    SYS_USB.Text := SYS_USB_Devices;

    for i := 0 to length(USB_Devices) - 1 do
      // для каждого устройства, указанного в параметрах запуска
    begin

      Dev := USB_Devices[i];
      find := False;
      // получить строки vendor и product
      getVendorProduct(DEV, vendor, product);

      // найти все порты, занятые данным устройством
      // Может оказаться несколько одинаковых устройств, надо найти порты для каждого
      for j := 0 to SYS_USB.Count - 1 do
        // перебор всех usb устройств в системе
      begin
        // устройство найдено среди системных устройств
        // (ищем подстроку среди всех параметров)
        if pos(Dev, SYS_USB[j]) <> 0 then
        begin
          // пометить найденным
          find := True;

          // получить bus и device найденного устройства
          getBusDevice(SYS_USB[j], bus, device);

          // Создать файл и получить его имя
          xmlFileName := CreatexmlFileName(KVM_Name, vendor, product, bus, device);

          // Добавить устройство из этого файла.
          Write('Добавление ' + vendor + ':' + product + '... ');
          res := DoProgram('virsh', 'attach-device ' + KVM_Name + ' ' + xmlFileName);

          if res = '' then
          begin
            res := 'Не удалось добавить ' + Dev;
          end;
          writeln(res);

        end;
      end;

      if not find then
        writeln('Не найдено в системе: ' + Dev);

    end;

    SYS_USB.Free;
  end;

begin
  // инициализация переменных
  Vars_Initialization;

  // инициализация файла настроек
  Conf_FileName := GetConfFileName;
  writeln('Используется файл конфигурации: ' +
    Conf_FileName);

  // Проверка корректности переданных параметров и завершение работы при наличии ошибок
  if ParserParamstr = False then
  begin
    // вывести помощь
    ShowHelp;
    exit;
  end;

  // запуск машины
  if DoProgram('virsh', 'start ' + KVM_Name) = '' then
    // при успешном запуске возвращается сообщение, при ошибке - пустая строка
  begin
    Writeln('Не удалось запустить виртуальную машину '
      + KVM_Name + '. Возможно, она уже запущена.');
  end;

  // Получение списка всех USB устройств системы
  SYS_USB_Devices := DoProgram('lsusb', '');

  // Подключение устройств к виртуальной машине
  AttachUSB;
end.
